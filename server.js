const express = require('express'),
      app = express(),
      bodyParser = require('body-parser');
const userFile = require('./data/user.json');

app.use(bodyParser.json());
const requestJSON=require('request-json');
var PORT = process.env.PORT || 3000;
const URL_BASE="/api.peru/v1/";
const mLabURLbase = 'https://api.mlab.com/api/1/databases/techu8db/collections/';
const apiKey = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';
/* operacion GET a todos los usuarios*/
/*
app.get(URL_BASE+'users',function(request,response){
  response.status(200).send(userFile);
});
*/
app.get(URL_BASE+'users/:id',function(request,response){
  let indice = request.params.id;
  let respuesta = (userFile[indice-1]==undefined) ? {"msg":"not found"} : userFile[indice-1];
  response.status(200).send(respuesta);
});

app.get(URL_BASE+'usersq',function(request,response){

  response.status(200);
  let max = request.query.max;
  if(userFile.length<max){
    max=userFile.length;
  }
  let respuesta=new Array();
  for (let i=0;i<max;i++){
    respuesta.push(userFile[i]);
  }
  response.send(respuesta);
});

app.get(URL_BASE+'usersC',function(request,response){
  let len = userFile.length;
  response.status(200).send({"num_elem" : len});
});

app.listen(PORT,function(){
  console.log("Node JS escuchando en el puerto "+ PORT);
});

app.post(URL_BASE + 'users',
    function(req, res) {
      // console.log(req.body);
      // console.log(req.body.id);
    let newUser = {
      userID: userFile.length+1,
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email,
      password: req.body.password
    }
    userFile.push(newUser);
    res.status(201);
    res.send({"mensaje":"Usuario creado correctamente",
              "usuario":newUser,
              "userFile actualizado":userFile});
});

app.put(URL_BASE + 'users/:id',
    function(req, res) {
      let indice = req.params.id;
      let encontrado=false;
    for (let i=0;i<userFile.length;i++){
      if(userFile[i].userID==indice){
        userFile[i].first_name= req.body.first_name;
        userFile[i].last_name= req.body.last_name;
        userFile[i].email= req.body.email;
        userFile[i].password= req.body.password;
        encontrado=true;
        break;
      }
    }
    let respuesta=
    encontrado ? {"mensaje":"Usuario actualizado ;D",
              "userFile actualizado":userFile}:{"mensaje":"Usuario no encontrado"};

    res.status(200).send(respuesta);
});

app.delete(URL_BASE +'users/:id',
    function(req, res) {
      let indice = req.params.id;
      let encontrado=false;
    for (let i=0;i<userFile.length;i++){
      if(userFile[i].userID==indice){
        userFile.splice(i,1);
        encontrado=true;
        break;
      }
    }
    let respuesta=
    encontrado ? {"mensaje":"Usuario eliminado ;)",
              "userFile actualizado":userFile}:{"mensaje":"Usuario no encontrado"};

    res.status(200).send(respuesta);

});

// LOGIN - user.json
 app.post(URL_BASE + 'login',
  function(request, response) {
   var user = request.body.email;
   var pass = request.body.password;
   for(us of userFile) {
    if(us.email == user && us.password == pass) {
      if(us.logged){
        response.send({"msg" : "already logged",
                "idUsuario" : us.userID});
      }else{
        us.logged = true;
        writeUserDataToFile(userFile);
        response.send({"msg" : "Login correcto.",
                "idUsuario" : us.userID,
                 "logged" : "true"});
      }

    }
   }
    response.send({"msg" : "Login incorrecto."});

 });


 // LOGOUT - users.json
 app.post(URL_BASE + 'logout',
  function(request, response) {
   //console.log("POST /apitechu/v1/logout");
   var userId = request.body.id;
   for(us of userFile) {
    if(us.userID == userId) {
     if(us.logged) {
       delete us.logged; // borramos propiedad 'logged'
     writeUserDataToFile(userFile);
     response.send({"msg" : "Logout correcto.", "idUsuario" : us.userID});
    } else {
     response.send({"msg" : "Logout incorrecto."});
    }
   }
  }
});

function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./data/user.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
    if(err) {
      console.log(err);
    } else {
     console.log("Datos escritos en 'user.json'.");
    }
   })
 }
