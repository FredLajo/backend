require('dotenv').config();
const express = require('express'),
      app = express(),
      bodyParser = require('body-parser');
const userFile = require('./data/user.json');

app.use(bodyParser.json());
const requestJSON=require('request-json');

const cors = require('cors');
app.use(cors());
app.options('*',cors());
var PORT = process.env.PORT || 3000;
const URL_BASE="/api.peru/v1/";
const mLabURLbase = process.env.MLAB_URL_BASE;
const apiKey = 'apiKey='+process.env.API_KEY;

app.listen(PORT,function(){
  console.log("Node JS escuchando en el puerto "+ PORT);
});


// GET users a través de mLab
app.get(URL_BASE + 'users',
  function(req, res) {
    const httpClient = requestJSON.createClient(mLabURLbase);
    //console.log("Cliente HTTP mLab creado.");
    const fieldParam = 'f={"_id":0}&';
    httpClient.get('user?' + fieldParam+ apiKey,
      function(err, respuestaMLab, body) {
        //console.log('Error: ' + err);
        //console.log('Respuesta MLab: ' + respuestaMLab);
        //console.log('Body: ' + body);
        var response = {};
        if(err) {
            response = {"msg" : "Error al recuperar users de mLab."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."};
            res.status(404);
          }
        }
        res.send(response);
      }); //httpClient.get(
}); //app.get(


  app.get(URL_BASE + 'users/:id',
    function(req, res) {
      const httpClient = requestJSON.createClient(mLabURLbase);
      const fieldParam = 'f={"_id":0}&';
      let ind = req.params.id;
      let queryString = 'q={"userID":'+ind+'}&';
      httpClient.get('user?'+queryString + fieldParam+ apiKey,
        function(err, respuestaMLab, body) {
          var response = {};
          if(err) {
              response = {"msg" : "Error al recuperar users de mLab."}
              res.status(500);
          } else {
            if(body.length > 0) {
              response = body;
            } else {
              response = {"msg" : "Usuario no encontrado."};
              res.status(404);
            }
          }
          res.send(response);
        });
  });



  app.get(URL_BASE + 'users/:id/accounts',
    function(req, res) {
      const httpClient = requestJSON.createClient(mLabURLbase);
      const fieldParam = 'f={"_id":0}&';
      let ind = req.params.id;
      let queryString = 'q={"userID":'+ind+'}&';
      httpClient.get('account?'+queryString + fieldParam+ apiKey,
        function(err, respuestaMLab, body) {
          var response = {};
          if(err) {
              response = {"msg" : "Error al recuperar users de mLab."}
              res.status(500);
          } else {
            if(body.length > 0) {
              response = body;
            } else {
              response = {"msg" : "Usuario no encontrado."};
              res.status(404);
            }
          }
          res.send(response);
        });
  });

  app.get(URL_BASE + 'users/accounts/:id/transactions',
    function(req, res) {
      const httpClient = requestJSON.createClient(mLabURLbase);
      const fieldParam = 'f={"_id":0}&';
      let ind = req.params.id;
      let queryString = 'q={"accountID":'+ind+'}&';
      httpClient.get('transaction?'+queryString + fieldParam+ apiKey,
        function(err, respuestaMLab, body) {
          var response = {};
          if(err) {
              response = {"msg" : "Error al recuperar users de mLab."}
              res.status(500);
          } else {
            if(body.length > 0) {
              response = body;
            } else {
              response = {"msg" : "Usuario no encontrado."};
              res.status(404);
            }
          }
          res.send(response);
        });
  });


  //Method POST login
  app.post(URL_BASE + "login",
    function (req, res){
      var email= req.body.email;
      var pass= req.body.password;
      var queryStringEmail='q={"email":"' + email + '"}&';
      var queryStringpass='q={"password":' + pass + '}&';
      var  clienteMlab = requestJSON.createClient(mLabURLbase);
      clienteMlab.get('user?'+ queryStringEmail+apiKey ,
      function(error, respuestaMLab , body) {
        //console.log("entro al body:" + body );
        var respuesta = body[0];
        //console.log(respuesta);
        if(respuesta!=undefined){
            if (respuesta.password == pass) {
              if(respuesta.logged==true){
                res.send([{"msg":"already logged"}]);
              }else{
                var session={"logged":true};
                var login = '{"$set":' + JSON.stringify(session) + '}';
                clienteMlab.put('user?q={"userID": ' + respuesta.userID + '}&' + apiKey, JSON.parse(login),
                 function(errorP, respuestaMLabP, bodyP) {
                  //res.send(body[0]);
                  getUserId(respuesta.userID,res);
                });
              }
            }
            else {
              res.send([{"msg":"contraseña incorrecta"}]);
            }
        }else{
          res.send([{"msg": "email Incorrecto"}]);
        }
      });
  });


  function getUserId(ind,res){
    const httpClient = requestJSON.createClient(mLabURLbase);
    const fieldParam = 'f={"_id":0}&';
    let queryString = 'q={"userID":'+ind+'}&';
    httpClient.get('user?'+queryString + fieldParam+ apiKey,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            response = [{"msg" : "Error al recuperar users de mLab."}]
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = [{"msg" : "Usuario no encontrado."}];
            res.status(404);
          }
        }
        res.send(response);
      });
  }

  //Method POST logout
  app.post(URL_BASE + "logout",
    function (req, res){
      var email= req.body.email;
      var queryStringEmail='q={"email":"' + email + '"}&';
      var  clienteMlab = requestJSON.createClient(mLabURLbase);
      clienteMlab.get('user?'+ queryStringEmail+apiKey ,
      function(error, respuestaMLab , body) {
        var respuesta = body[0];
        if(respuesta!=undefined){
              if(respuesta.logged!=true){
                res.send({"msg": "user not logged"});
              }else{
                var session={"logged":true};
                var logout = '{"$unset":' + JSON.stringify(session) + '}';
                clienteMlab.put('user?q={"userID": ' + respuesta.userID + '}&' + apiKey, JSON.parse(logout),
                 function(errorP, respuestaMLabP, bodyP) {
                  //res.send(body[0]);
                  //res.send({"msg": "logout Exitoso"});
                  getUserId(respuesta.userID,res);
                });
              }

        }else{
          res.send({"msg": "Error en logout"});
        }
      });
  });


/*
  app.get(URL_BASE + 'userAccounts/:id',
    function(req, res) {
      const httpClient = requestJSON.createClient(mLabURLbase);
      const fieldParam = 'f={"_id":0}&';
      let ind = req.params.id;
      let queryString = 'q={"idUser":'+ind+'}&';
      httpClient.get('userAccount?'+queryString + fieldParam+ apiKey,
        function(err, respuestaMLab, body) {
          var response = {};
          if(err) {
              response = {"msg" : "Error al recuperar users de mLab."}
              res.status(500);
          } else {
            if(body.length > 0) {
              response = body;
            } else {
              response = {"msg" : "Usuario no encontrado."};
              res.status(404);
            }
          }
          res.send(response);
        });
  });*/
