# Imagen inicial
FROM node:latest

# crear directorio de trabajo del contenedor docker
WORKDIR /docker-api-fred

# copiar los archivos necesarios para q la aplicacion funcione
ADD . /docker-api-fred

#instalar dependencias de produccion
#RUN npm install --only=production

#exponer un puerto escucha del contenedor (mismo definido en API REST)
EXPOSE 3000

#lanzar los comandos mecesarios para ejecutar nuestra API
CMD ["npm","run","pro"]
